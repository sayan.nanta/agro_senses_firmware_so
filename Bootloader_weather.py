## pi@raspberrypi:~ $ crontab -e
## @reboot python3 agro_senses_firmware_so/clear_io.py
## @reboot python3 agro_senses_firmware_so/Bootloader.py
## @reboot python3 agro_senses_firmware_so/BootWatchdog.py

import subprocess
import os
# import lamp_ctl
# import ipcam_ctl
# import router_ctl
import weather_ctl
import Weather_service
import time
import timerService
import energy_mesure
import _thread
# import MqttService as MqttService
import SendWeatherData
import bacup_data_service
import Constants
import Davin_Env
import uuid
import check_connection
import LogService as LogService
log_service = LogService.LogService()

####### Set I/O on boot
# ipcam_ctl.set_state(False)
weather_ctl.set_state(False)
# lamp_ctl.set_state(False)
# router_ctl.set_state(True)
# time.sleep(15)

def check_inet():
    #### Check Internet ####
    if check_connection.checkInternetHttplib():
        Constants.INET_CON = True
        return True
    else:
        Constants.INET_CON = False
        return False

try:
    os.system('pwd')
    os.chdir("agro_senses_firmware_so")
    os.system('pwd')
except:
    pass

class main:
    def __init__(self):
        print(Constants.SOFTWARE_TITLE)
        time.sleep(1)
        weather_ctl.set_state(True)
        try:
            if Davin_Env.STATION_INDEX == '0':
                if Davin_Env.WEATHER_SERVICE == True:
                    _thread.start_new_thread(Weather_service.SerialPortService, ())
                    log_service.info('-Run Weather_service')
                    print('-Run Weather_service')

                    _thread.start_new_thread(SendWeatherData.SendWeatherData, ())
                    log_service.info('-### Run Send Weather DataS ervice')
                    print('\n-### Run SendDataService')

                    _thread.start_new_thread(bacup_data_service.BackupData, ())
                    log_service.info('-### Run Bacup data service')
                    print('\n-### Run Bacup data service')
            else:
                print('Disable weather service')
                log_service.info('Disable weatherservice')

            if Davin_Env.STATION_INDEX == '0':
                if Davin_Env.ENERGY_SERVICE == True:
                    energy_mesure.mesure()
                    log_service.info('-Run energy_mesure')
                    print('-Run energy_mesure')
                    _thread.start_new_thread(energy_mesure.mesure.read(), ())
                else:
                    print('Disable Energy service')
                    log_service.info('Disable Energy service')
                    
        except Exception as e:
            print('!!!! Err:{}'.format(e))
            log_service.info('!!!! Err:{}'.format(e))

        while True:
            time.sleep(60)
            print('Main loop')
        print('Exit Bootloader main')

if __name__ == '__main__':
    main()